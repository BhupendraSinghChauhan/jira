package com.jira.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.Augmenter;

public class Page {
 
	public static WebDriver driver=null;
	public static Properties config=null;
	//public static WebDriverWait wait = new WebDriverWait(driver, 30);
	public String emptyString="";
	
	public Page()
	{
		if(driver==null)
			{
				this.driver=new FirefoxDriver();
				//driver.manage().window().maximize();
				config=new Properties();
				try{																		
					FileInputStream fin=new FileInputStream(System.getProperty("user.dir")+"\\src\\com\\jira\\config\\config.properties");
					config.load(fin);
					driver.get(config.getProperty("testSiteName"));
				}catch(Exception ex)
				{
					ex.printStackTrace();
					return;
				}
				
			}
	}
	
	
	//Click Method
	
		public void click(String LinkTextKey,String idKey)
		{
			try{
				if(LinkTextKey.length()!=0)
				{
					driver.findElement(By.linkText(LinkTextKey)).click();
				}else
				{
					driver.findElement(By.id(idKey)).click();
				}
				
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
		}
		
		//Input Method
		
		public void input(String idKey,String text)
		{
			try{
				
				if(text.length()!=0)
					{
						driver.findElement(By.id(idKey)).clear();
						driver.findElement(By.id(idKey)).sendKeys(text);
					}else{
						driver.findElement(By.id(idKey)).submit();
					}
				
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
		}
		
		//Verification
		
		public boolean isElementPresent(String idKey,String bug_Id)
		{
			try{
				String bug_text=driver.findElement(By.id(idKey)).getText();
				String[] bug_actual=bug_text.split("-");
				
				if(bug_actual[1].equals(bug_Id))
				{
					return true;
				}
				
			}catch(Exception ex){
				return false;
			}
			
			return false;
		}
	
		public static String current_date_generator()
		{
			DateFormat dateFormat = new SimpleDateFormat("dd/MMM/yy");
			Date date = new Date();
			return dateFormat.format(date);
		}
		
		public static void takeScreenShots(String fileName) throws IOException
		{
			WebDriver augmentedDriver = new Augmenter().augment(driver); 
			File source =((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE); 
			String path = (System.getProperty("user.dir")+"\\screenShots\\"+fileName+".jpg");
	        FileUtils.copyFile(source, new File(path)); 

			
		}
	
}
