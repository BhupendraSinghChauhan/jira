package com.jira.base;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.jira.dashboard.LandingPage;
import com.jira.login.LoginPage;

@RunWith(Suite.class)
@SuiteClasses({
	LoginPage.class,
	LandingPage.class,
	
	})
public class TestSuite extends Page {
	
	@AfterClass
	public static void stop()
	{
		
		//driver.quit();
	}
	

	
}

