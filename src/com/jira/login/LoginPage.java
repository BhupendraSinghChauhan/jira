package com.jira.login;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.jira.base.Page;
import com.jira.dashboard.LandingPage;

public class LoginPage extends Page{
	
	public LandingPage doLogin(String userName, String password)
	{
		
		click("Log In",emptyString);
		input("username",userName);
		input("password", password);
		click(emptyString,"login-submit");
		
		WebElement element = LandingPage.wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Close")));
		
		element.click();
		
		return new LandingPage();
		
	}
		
	@Test
	public void LoginTest()  
	{
		doLogin(config.getProperty("userName"), config.getProperty("password"));
	}
	
}
