package com.jira.dashboard;

import java.io.IOException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.jira.base.Page;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class LandingPage extends Page{
		
	public static WebDriverWait wait = new WebDriverWait(driver, 30);
	public static String[] str={};
	public static String bug_Id="";
	
	@Test
	public void AddBug()
	{
		
		System.out.println("hello welcome to POM ...");
		try{
			createBug();
		}catch(Exception ex)
		{
			ex.getStackTrace();
		}
		
		System.out.println("hello welcome to POM 122...");
	}
	
	
	
	//String issueId="TST-56572";
	
	@Test
	public void searchIssue() throws IOException
	{
		input("quickSearchInput",bug_Id);
		input("quickSearchInput",emptyString);
		if(isElementPresent("key-val",bug_Id))
		{
			
		}else
		{
			takeScreenShots("searchIssue is not working properly ...");
		}
		
	}
	
	@Test
	public void updateIssue() throws IOException
	{
		input("quickSearchInput",bug_Id);
		input("quickSearchInput",emptyString);
		if(isElementPresent("key-val",bug_Id))
		{
			
		}else
		{
			takeScreenShots("searchIssue is not working properly ...");
		}
		
		//String priority_text="Major";
		click(emptyString, "edit-issue");
		
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("description")));
		
		String Summary_text=RandomStringUtils.randomAlphabetic(30);
		input("summary",Summary_text);
		
		click(emptyString, "edit-issue-submit");
		
		String description_text_actual=driver.findElement(By.id("summary-val")).getText();
		
		if(description_text_actual.equals(Summary_text))
		{
			
		}else
		{
			takeScreenShots("Update functionality is not working properly ...");
		}
		
	}
	
	public  String createBug() throws InterruptedException
	{
		
		System.out.println("hello welcome to POM ...");
		
		click(emptyString,"create_link");
		
		WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement create_issue_popup = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("project-field")));
		
		String Summary_text=RandomStringUtils.randomAlphabetic(30);
		
		input("summary",Summary_text);
		
		String Security_Level_txt="Logged-in users";
		Select  Security_Level_DDL=new Select(driver.findElement(By.id("security")));
		Security_Level_DDL.selectByVisibleText(Security_Level_txt);
		
		String Priority_txt="Trivial";
		
		input("priority-field",Priority_txt);
		
		
		
		String Assignee_txt="Unassigned";
		input("assignee-field",Assignee_txt);
		
		String Environment_txt="environment test to test SAMPLE Project ";
		input("environment",Environment_txt);
		
		String Description_txt="Description Test to test SAMPLE Project ";
		input("description",Description_txt);
		
		input("customfield_10041",Page.current_date_generator());
		
		String Original_Estimate_txt="5w 5d 10h";
		input("timetracking",Original_Estimate_txt);
		
		
		String Labels_txt="support";
		input("labels-textarea",Labels_txt);
		
		
		
		click(emptyString,"create-issue-submit");
		Thread.sleep(2000);
		WebElement issue_Id = wait.until(ExpectedConditions.presenceOfElementLocated(By.partialLinkText("TST-")));
		String str1=issue_Id.getAttribute("href");
		
		String[] str=str1.split("-");
		bug_Id=str[1];
		return str[1];
	}
	
}
